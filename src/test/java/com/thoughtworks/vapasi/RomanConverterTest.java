package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;


class RomanConverterTest {

    private RomanConverter romanConvertor;

    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    }



    @ParameterizedTest
    @CsvSource({"I,1", "II,2", "III,3","IV,4","V,5","VI,6","VII,7","IX,9","X,10","XXXVI,36","MMXII,2012","MCMXCVI,1996"})
    void shouldConvertRomantoArabic(String input, Integer expected) {
        Integer actualValue = romanConvertor.convertRomanToArabicNumber(input);
        assertEquals(expected, actualValue);
    }


    @Test
    void shouldReturnMinusone()
    {
        assertEquals(-1,romanConvertor.convertRomanToArabicNumber("S"));
    }

   @Test
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {
        Assertions.assertThrows(IllegalArgumentException.class,()->romanConvertor.convertRomanToArabicNumber(null),
                "Invalid Input String");
    }
}