package com.thoughtworks.vapasi;

public class RomanConverter {


    public Integer convertRomanToArabicNumber(String roman) {
        if(roman == null)
            throw new IllegalArgumentException("Invalid Input String");
        int arabicNumber = 0;
        for (int i = 0; i < roman.length(); i++) {

            int currentNumber = getNumberForRoman(roman.charAt(i));
            if (i + 1 < roman.length()) {

                int nextNumber = getNumberForRoman(roman.charAt(i + 1));
                if (currentNumber >= nextNumber) {
                    arabicNumber += currentNumber;
                } else {
                    arabicNumber = arabicNumber + nextNumber - currentNumber;
                    i++;
                }
            } else {
                arabicNumber += currentNumber;
            }
        }
        int result = (arabicNumber <= 0) ? -1 : arabicNumber;
        return result;


    }

    public int getNumberForRoman(char c) {
        int result = 0;
        switch (c) {

            case 'I':
                result = 1;
                break;
            case 'V':
                result = 5;
                break;
            case 'X':
                result = 10;
                break;
            case 'C':
                result = 100;
                break;
            case 'D':
                result = 500;
                break;
            case 'M':
                result = 1000;
                break;


        }
        return result;
    }

}
